Source: libpath-isdev-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Florian Schlichting <fsfs@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13), perl
Build-Depends-Indep: libclass-tiny-perl,
                     libfile-homedir-perl,
                     libmodule-runtime-perl,
                     libpath-tiny-perl,
                     librole-tiny-perl,
                     libsub-exporter-perl,
                     libtest-fatal-perl,
                     perl | libtest-simple-perl
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libpath-isdev-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libpath-isdev-perl.git
Homepage: https://metacpan.org/release/Path-IsDev

Package: libpath-isdev-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libclass-tiny-perl,
         libfile-homedir-perl,
         libmodule-runtime-perl,
         libpath-tiny-perl,
         librole-tiny-perl,
         libsub-exporter-perl
Description: Perl module to determine if a given Path resembles a development source tree
 Path::IsDev is more or less a bunch of heuristics for determining if a
 given path is a development tree root of some kind. This has many
 useful applications, notably ones that require behaviours for
 "installed" modules to be different to those that are still "in
 development".
